// 头部
  
  var app = new Vue({
    el: '.header_right',
    data: {
      index: '主页',
      product:'产品信息',
      support:'技术支持',
      about:'关于我们',
      en:'English'
    }
  })

  var shouji = new Vue({
  	el: '.daohang',
    data: {
      index: '主页',
      product:'产品信息',
      support:'技术支持',
      about:'关于我们',
      en:'English'
    }
  })



//特换产品介绍
  $('.canpin_qian ul li:eq(0)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>适用于智能商场、无人仓储物流等商业级智能应用场景。</p>'+
      '<p>最高速度：1.2m/s</p>'+ 
      '<p>定位精度：<3cm</p>'+
      '<p>最大载重：40kg</p>'+
      '<p>最大续航：5小时</p>'+
      '<p>最大爬坡：15°</p>'
    )

  });

  $('.canpin_qian ul li:eq(1)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>家庭用全智能草坪机器人，一键剪草，省心省力。</p>'+ 
      '<p>智能全区域覆盖路线规划</p>'+ 
      '<p>自主避障</p>'+ 
      '<p>用户地图操控界面</p>'+ 
      '<p>集成邦鼓思自研高精度定位技术</p>'+ 
      '<p>精准刀盘控制</p> <p>20度最大爬坡力</p>'
    )

  });

  $('.canpin_qian ul li:eq(2)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>极短的RTK收敛时间和可靠的厘米级精度解算输出。可双天线输入，提供精密航向。</p>'+ 
      '<p>水平位置精度（SPP模式下的CEP50）：2.5cm</p>'+ 
      '<p>授时精度：20 ns RMS</p>'+ 
      '<p>实时动态（RTK精度1σ）</p>'+ 
      '<p>水平：0.008m+1pp,</p>'+ 
      '<p>垂直：0.015m+1ppm</p>'+ 
      '<p>RTK初始化时间<45s</p>'+ 
      '<p>重捕获时间<2s</p>'
    )

  });

  $('.canpin_qian ul li:eq(3)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>广泛应用于道路施工、海洋测量、码头集装箱作业、边防警戒、室外机器人定位等场合。</p>'+ 
      '<p>工作频率：BeiDou B1/B2/B3，GPS L1/L2，GLONASS G1/G2</p>'+ 
      '<p>极化方式：RHCP</p>'+ 
      '<p>输出阻抗：50Ω</p>'+ 
      '<p>接头方式：THC-K</p>'+  
      '<p>尺寸：D 200mm H 68mm</p>'
    )

  });

  $('.canpin_qian ul li:eq(4)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>极短的RTK收敛时间和厘米级精度解算输出。使用先进的MEMS传感器融合技术、板载</p>'+ 
      '<p>MEMS IMU和磁力计、实时时钟（RTC）用于快速热启动。</p>'+  
      '<p>水平位置精度（SPP模式下的CEP50）：2.5cm</p>'+  
      '<p>授时精度：20 ns RMS</p>'+  
      '<p>实时动态（RTK精度1σ）</p>'+  
      '<p>水平：0.015m+1pp,</p>'
    )

  });

  $('.canpin_qian ul li:eq(5)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>可靠的室外分米级定位精度民用方案。</p>'+
      '<p>水平位置精度（SPP模式下的CEP50）：5cm</p>'+
      '<p>授时精度：20 ns RMS</p>'+
      '<p>实时动态（RTK精度1σ）</p>'+
      '<p>水平：0.05m+1pp,</p>'+
      '<p>垂直：0.10m+1ppm</p>'+
      '<p>RTK初始化时间<5min</p>'
    )

  });

  $('.canpin_qian ul li:eq(6)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>通讯距离50km，耗能小、在任何环境条件下能获得可靠、无误码率的数据流。</p>'+
      '<p>接口类型：5VDC, SMA-K </p>'+
      '<p>工作频率：902-928MHz</p>'+
      '<p>发射功率：10mW至1W可调</p>'+
      '<p>发射电流：最大855mA</p>'+
      '<p>空中速率：最高支持153.6kbps</p>'+
      '<p>串口速率：最高支持115200bps</p>'+
      '<p>功能特点：全双工；前向纠错；自动跳频，最大支持112个跳频信道</p>'
    )

  });

  $('.canpin_qian ul li:eq(7)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>自研数据融合算法，易于实现机器人定位、导航、路径规划，易于二次开发。</p>'+
      '<p>供电：12~24V</p>'+
      '<p>功耗：3w超低</p>'+
      '<p>接口：2x百兆网口、2xUSB2.0、DB9串口、4G和wifi模块</p>'+
      '<p>通信总线：4xRS232、4×RS485总线、2xCAN总线</p>'+
      '<p>可连接Rik高精度厘米级定位模块和IMU九轴传感器</p>'+
      '<p>配套BongOS 软件SDK</p>'
    )

  });

  $('.canpin_qian ul li:eq(8)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>使用邦鼓思自行研发的数据融合算法，为机器人定位、导航、路径规划的实现提供了极大便利，同时集成人工智能算法IP，易于工程师二次开发。</p>'+
      '<p>供电：12~24V</p>'+
      '<p>功耗：3w超低</p>'+
      '<p>接口：2x千兆网口、4xUSB2.0、3xUART、4G和wifi模块、2xCAN总线</p>'
    )

  });

  $('.canpin_qian ul li:eq(9)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>使用2G数据网络，流量消耗性，功耗低，实时连接GNSS CORS站，低成本代替用户架设基站的支出。</p>'+
      '<p>接口类型：12VDC SMA-K RS232 RJ45</p>'+
      '<p>工作频率：GSM GPRS</p>'+
      '<p>发射功率：最大5W</p>'+
      '<p>出口速率：支持最高115200bps</p>'+
      '<p>功能特点：支持RTCM3.2, 支持RTCM2.3等多种常用CORS协议</p>'
    )

  });








  
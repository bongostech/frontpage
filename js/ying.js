// 头部
  
  var app = new Vue({
    el: '.header_right',
    data: {
      index: 'Home',
      product:'Products',
      support:'Supports',
      about:'About us',
      cn:'中文'
    }
  })

  var shouji = new Vue({
  	el: '.daohang',
    data: {
      index: 'Home',
      product:'Products',
      support:'Supports',
      about:'About us',
      cn:'中文'
    }
  })

//特换产品介绍
  $('.canpin_qian ul li:eq(0)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Suitable for smart shopping malls, unmanned warehouses and other intelligent commercial scenarios.</p>'+
      '<p>Max Speed: 1.2m/s</p>'+ 
      '<p>Positioning Accuracy <3cm</p>'+
      '<p>Max Load: 40kg</p>'+
      '<p>Max Working Time: 5 hours</p>'+
      '<p>Max Climbing Angle: 15°</p>'
    )
  });
  $('.canpin_qian ul li:eq(1)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Next generation smart lawn mower provides you a perfect lawn care</p>'+ 
      '<p>Smart coverage planning for all kinds of areas</p>'+ 
      '<p>Autonomous obstacle avoidance</p>'+ 
      '<p>User interface and robot control panel</p>'+ 
      '<p>Integrated with Bongos’ high-precision positioning technology</p>'+ 
      '<p>Precise cutter control technology</p>'+ 
      '<p>Maximum climbing force up to 20 degree</p>'
    )
  });
  $('.canpin_qian ul li:eq(2)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Provides the rapid RTK convergence rate and reliable centimeter accuracy solution outputs. Supports dual antenna inputs for precise heading.</p>'+ 
      '<p>Horizontal position accuracy (CEP 50 in SPP mode): 2.5cm</p>'+ 
      '<p>Timing accuracy: 20 ns RMS</p>'+ 
      '<p>Real-time dynamic (RTK accuracy 1σ）</p>'+ 
      '<p>Horizontal: 0.008m + 1ppm</p>'+ 
      '<p>Vertical: 0.015m + 1ppm</p>'+ 
      '<p>RTK initialization time <45s</p>'+ 
      '<p>Recapture time < 2s</p>'
    )
  });
  $('.canpin_qian ul li:eq(3)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Widely used in road construction, marine surveying, terminal container operation, border guard, outdoor robot positioning and other occasions.</p>'+ 
      '<p>Working Frequency: BEIDOU2 B1/B2/B3, GPS L1/L2, GLONASS G1/G2</p>'+ 
      '<p>Polarization: RHCP Output</p>'+ 
      '<p>Impedance: 50Ω</p>'+ 
      '<p>Joint Interface: TNC-K</p>'+  
      '<p>Size: D 200mm H 68mm</p>'
    )
  });
  $('.canpin_qian ul li:eq(4)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Provide the rapid RTK convergence rate and reliable centimeter accuracy solution outputs. Advanced MEMS sensor fusion technology, onboard MEMS IMU and magnetometer, real-time clock (RTC) for fast hot start.</p>'+ 
      '<p>Horizontal position accuracy (CEP 50 in SPP mode): 2. 5cm</p>'+  
      '<p>Timing accuracy: 20 ns RMS</p>'+  
      '<p>Real-time dynamic (RTK accuracy 1σ）</p>'+  
      '<p>Horizontal: 0.015m + 1ppm</p>'+ 
      '<p>Vertical: 0.030m + 1ppm</p>'+ 
      '<p>RTK initialization time <60s</p>'
    )
  });
  $('.canpin_qian ul li:eq(5)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Provide reliable decimeter-level positioning accuracy for outdoor solutions.</p>'+
      '<p>Horizontal position accuracy (CEP 50 in SPP mode): 5cm</p>'+
      '<p>Speed accuracy: 0.20 m / s RMS</p>'+
      '<p>Real-time dynamic (RTK accuracy 1σ）</p>'+
      '<p>Horizontal: 0.05m + 1ppm</p>'+
      '<p>Vertical: 0.10m + 1ppm</p>'+
      '<p>RTK initialization time <5min</p>'
    )
  });
  $('.canpin_qian ul li:eq(6)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Products are high-energy efficiency and long transmission up to 50km. This product undergoes a rigorous, highly reliable test to ensure a reliable, error-free data flow under any conditions and in any environment. </p>'+
      '<p>Interface Type: 5VDC, SMA-K</p>'+
      '<p>Working Frequency: 902—928MHz</p>'+
      '<p>Transmit Power: 10mW to 1W</p>'+
      '<p>Emission Current: Maximum 855mA</p>'+
      '<p>Air Rate: Maximum support 153.6kbps</p>'+
      '<p>Serial speed: Maximum support 115200bps</p>'+
      '<p>Features: Full duplex, forward error correction, automatic frequency hopping, maximum support 112 frequency hopping channel.</p>'
    )
  });
  $('.canpin_qian ul li:eq(7)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>The self-developed data fusion algorithm greatly facilitates the robot positioning, navigation and path planning. Easier for further developments.</p>'+
      '<p>Power: 12~24V</p>'+
      '<p>Power Consumption: 3w</p>'+
      '<p>Interface: 2x Fast ethernet port, 2×USB2.0, bd9 serial port, 4G and wifi module</p>'+
      '<p>Communication Bus: 4×RS232, 4×RS485 bus, 2×CAN bus</p>'+
      '<p>Built-in Rik high-precision centimeter-level positioning module and IMU nine-axis sensor</p>'+
      '<p>Assort with BongOS SDK.</p>'
    )
  });
  $('.canpin_qian ul li:eq(8)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Self-developed data fusion algorithm helps robot positioning, navigation, path planning better. Integrated artificial intelligence algorithm IP, easy for further development.</p>'+
      '<p>Power: 12~24V</p>'+
      '<p>Power Consumption: 3w</p>'+
      '<p>Interface: 2x Fast ethernet port, 4×USB2.0, 3xUART, 4G and wifi module, 2×CAN bus</p>'
    )
  });
  $('.canpin_qian ul li:eq(9)').click(function(event) {
    $('.tanc_canp').find('.tanc_canp_nei_right .canq_jies').html(
      '<p>Use 2G data network, low traffic and power consumption, real-time connection GNSS CORS station, a cheaper solution than setting up a base station.</p>'+
      '<p>Interface Type: 12VDC SMA-K RS232 RJ45</p>'+
      '<p>Working Frequency: GSM GPRS</p>'+
      '<p>Transmit Power: Maximum5W</p>'+
      '<p>Export Rate: Maximum support 115200bps</p>'+
      '<p>Features: Support RTCM3.2, RTCM2.3, and other commonly used CORS protocol</p>'
    )
  });










  